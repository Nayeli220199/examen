create database abarrotespueblito;
use abarrotespueblito;

create table proveedores(id_proveedor int primary key auto_increment, nombre varchar(100), direccion varchar(50), telefono varchar(10));
create table productos(id_productos int primary key auto_increment, nombrep varchar(100), 
precio int, fkcategorias int, foreign key (fkcategorias) references categorias (id_categorias));
create table categorias(id_categorias int primary key auto_increment, nombre varchar (100));

create table compras (id_compras int primary key auto_increment, cantidad double, fkproductos int, fkproveedores int, fecha_compra date,
foreign key (fkproductos) references productos (id_productos), foreign key (fkproveedores) references proveedores (id_proveedor));

insert into proveedores values(1,'Arturo','Hermion Larios','4741057890');
insert into productos values(12,'pan dulce','5','2');
insert into categorias values (2,'Panaderia');
insert into compras values (5,'10','12','2','14/febrero/2020');

create view v_vista as select nombrep, precio, nombre,cantidad  from productos, compras, proveedores 
where fkproductos = id_productos and fkproveedores =id_proveedor;


show tables;
create procedure Pedido (in id_proveedor int, id_producto int, id_compras int, 
in cantidad double, in fecha_compra date)
begin 
if cantidad is null then 
select "no es corresto, no puede ser nula";
else
insert into compras  values(id_proveedor, id_producto, id_compras, cantidad, fecha_compra);
select "Correcto";
end if;
end;

call Pedido(null,1,12,2,5,'14/febrero/2020');

select nombrep,nombre from Productos,Categorias where id_Categorias = fkid_Categorias order by nombre asc;